import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

// let baseUrl = process.env.NODE_ENV === 'production' ? 'http://' + window.location.hostname + ':11001' : window.location.origin
// let baseUrl = process.env.VUE_APP_BASEURI

export const state = {

  // Login
  authenticated: false,
  profile: []
  // End Login
}

export const getters = {
  AUTHENTICATED: state => {
    return state.authenticated;
  },
  PROFILE: state => {
    return state.profile;
  }
}

export const mutations = {
  SET_AUTHENTICATED(state) {
    state.authenticated = true
  },
  SET_UNAUTHENTICATED(state) {
    state.authenticated = false,
      state.profile = []
  },
  SET_PROFILE(state, resp) {
    state.profile = resp
    console.log("uhuuy", state.profile);
  }
}

export const actions = {
  GET_AUTHENTICATED({ commit }, { username, password }) {
    return new Promise((resolve, reject) => {
      Axios.post('oauth/token?grant_type=password&username=' + username + '&password=' + password,
        {
          params: {
            username: 'myclientId', password: 'mysecret'
          }
        }
      ).then((response) => {
        console.log('isi response', response)
        commit("SET_PROFILE", response.data.access_token);
        commit("SET_AUTHENTICATED");
        resolve(response);
      })
        .catch(error => {
          console.log('isi error', error)
          reject(error)
        });
    })
  }
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

// export default new Vuex.Store({
//   state: {
//     status: '',
//     token: '',
//   },
//   mutations: {
//     auth_success(state, token) {
//       state.status = 'success'
//       state.token = token
//     },
//     auth_error(state) {
//       state.status = 'error'
//     },
//     logout(state) {
//       state.status = ''
//       state.token = ''
//     }
//   },
//   actions: {
//     loginUser({ commit }, { username, password }) {
//       return new Promise((resolve, reject) => {
//         Axios.post(baseUrl + 'oauth/token?grant_type=password&username=' + username + '&password=' + password),
//           {
//             auth: {
//               username: "myclientId",
//               password: "mysecret",
//             },
//           }
//             .then(response => {
//               console.log('ini response', response)
//               commit('auth_success', response.data)
//               resolve(response)
//             })
//             .catch(err => {
//               commit('auth_error', err)
//               reject(err)
//             })
//       })
//     }
//   }
// })
